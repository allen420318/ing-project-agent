import webpack from "webpack"
import HtmlwebpackPlugin from "html-webpack-plugin"
import VueLoaderPlugin from "vue-loader/lib/plugin"
import MiniCssExtractPlugin from "mini-css-extract-plugin"
import CopyPlugin from "copy-webpack-plugin"
import { CleanWebpackPlugin } from "clean-webpack-plugin"
import { BundleAnalyzerPlugin } from "webpack-bundle-analyzer"
import TerserPlugin from "terser-webpack-plugin"
import OptimizeCSSAssetsPlugin from "optimize-css-assets-webpack-plugin"
import path from "path"

const isProduction = process.env.NODE_ENV === "production"
const webType = process.env.webtype

const configMenu = require(`./src/configs/${webType}/menu.js`).default
const configMeta = require(`./src/configs/${webType}/meta.js`).default

console.log({ isProduction, webType })

const distDir = path.resolve(`./dist/${webType}`)

const config = {
  mode: isProduction ? "production" : "development",
  devtool: false,
  entry: { index: "./src/main.js" },
  devServer: {
    disableHostCheck: true,
    publicPath: "/",
    hot: true,
    historyApiFallback: { disableDotRule: true },
    headers: {
      "Access-Control-Allow-Origin": "*",
      "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE, PATCH, OPTIONS",
      "Access-Control-Allow-Headers": "X-Requested-With, content-type, Authorization"
    }
  },
  stats: {
    colors: true,
    children: false,
    chunks: false,
    modules: false
  },
  output: {
    path: distDir,
    publicPath: "/",
    filename: isProduction === true ? "js/[name].[chunkhash:16].js" : "js/[name].[hash].js",
    chunkFilename: isProduction === true ? "js/[name].[chunkhash:16].js" : "js/[name].[hash].js"
  },
  module: {
    rules: [
      {
        test: /\.css$/,
        use: [
          {
            loader: MiniCssExtractPlugin.loader,
            options: {
              esModule: true
            }
          },
          { loader: "css-loader" }
        ]
      },
      {
        test: /\.styl(us)?$/,
        use: [
          {
            loader: MiniCssExtractPlugin.loader,
            options: {
              esModule: true
            }
          },
          { loader: "css-loader" },
          { loader: "stylus-loader" }
        ]
      },
      {
        test: /\.(eot|svg|ttf|woff|woff2)$/,
        use: { loader: "file-loader" }
      },
      {
        test: /\.(jpg|png|gif)$/,
        use: [
          {
            loader: "url-loader",
            options: {
              name: "img/[name].[ext]",
              limit: "300"
            }
          }
        ]
      },
      {
        test: /\.js$/,
        exclude: /node_modules/,
        loader: "babel-loader"
      },
      {
        test: /\.pug$/,
        oneOf: [
          // this applies to `<template lang="pug">` in Vue components
          {
            resourceQuery: /^\?vue/,
            use: ["pug-plain-loader"]
          },
          // this applies to pug imports inside JavaScript
          {
            use: ["raw-loader", "pug-plain-loader"]
          }
        ]
      },
      { test: /\.vue$/, loader: "vue-loader" }
    ]
  },
  resolve: {
    alias: {
      vue$: "vue/dist/vue.runtime.esm.js",
      "@": path.join(__dirname, "src"),
      "vee-validate$": "vee-validate/dist/vee-validate.min.js",
      img: path.join(__dirname, "src/img")
    },
    extensions: [".js", ".vue"]
  },
  plugins: [
    new webpack.SourceMapDevToolPlugin({}),
    new CleanWebpackPlugin({}),
    new VueLoaderPlugin(),
    new MiniCssExtractPlugin({ filename: "css/[name].[contenthash:16].css" }),
    new CopyPlugin({
      patterns: [
        { from: "./version.txt", to: "./" },
        { from: "./src/media", to: "./media" },
        { from: "./src/img", to: "./img" },
        { from: "./src/i18n", to: "./i18n" }
      ]
    }),
    new HtmlwebpackPlugin({
      title: "",
      filename: "index.html",
      template: "./src/index.html",
      inject: true
    })
  ],
  optimization: {
    minimize: isProduction,
    minimizer: [new TerserPlugin({ cache: true, parallel: true, sourceMap: false }), new OptimizeCSSAssetsPlugin({})],
    runtimeChunk: "single",
    splitChunks: {
      cacheGroups: {
        vendors: { test: /[\\/]node_modules[\\/]/, name: "vendors", chunks: "initial", enforce: true },
        commons: {
          test: /[\\/]node_modules[\\/]/,
          minChunks: 2,
          name: "commons",
          chunks: "all",
          priority: 10,
          reuseExistingChunk: true
        }
      }
    }
  }
}

config.plugins.push(
  new webpack.DefinePlugin({
    __BuildVar__: {
      webType: JSON.stringify(webType),
      menu: JSON.stringify(configMenu.menu),
      meta: JSON.stringify(configMeta)
    }
  })
)

if (isProduction === true) {
  config.plugins.push(new BundleAnalyzerPlugin({ analyzerMode: "static", reportFilename: "./analyze.html" }))
}

export default config
