import { MessageBox } from "element-ui"

export const ErrorMessageMixin = {
  methods: {
    parseErrorMessage(backendMessage) {
      const ErrorMessageMap = this.$store.getters["i18n/apiErrorKeyMapper"]
      const defaultMessage = "後端資料錯誤"

      try {
        const msg = backendMessage.data.message

        if (typeof msg === "string") {
          const key = decodeURIComponent(JSON.parse(msg).sub)
          const result = ErrorMessageMap.get(key) || defaultMessage
          return result
        } else {
          // direct output
          return msg
        }
      } catch (err) {
        // if encounter error , direct output default message
        return defaultMessage
      }
    },
    showErrorMessage(error, callback) {
      MessageBox.alert(error.message, {
        showClose: false,
        customClass: "error-message-box__global",
        showCancelButton: false,
        confirmButtonText: "確定",
        closeOnPressEscape: false,
        center: true,
        callback: callback
      })
    }
  }
}

export default ErrorMessageMixin
