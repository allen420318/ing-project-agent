import {
  getDateWithStartTime,
  getDateWithEndTime,
  getPreviousDateByDays,
  getDaysBetweenDates,
  getWithinDaysDate
} from "@/libs/time.js"

export const watch = {
  watch: {
    dateRangeDays: function(value) {
      const absValue = Math.abs(value)
      const startDate = this.current.startDate
      const endDate = this.current.endDate
      const withinRangeDate = getWithinDaysDate(startDate, 7)
      const isStartDateLaterThanEndDate = value > 0
      const isRangeInSameDay = absValue < 1
      const isRangeBreakLimit = absValue > 7

      if (isRangeBreakLimit === true || (isStartDateLaterThanEndDate === true) & (isRangeInSameDay === false)) {
        this.$refs["endDate"].setDate(withinRangeDate)
      } else if ((isStartDateLaterThanEndDate === true) & (isRangeInSameDay === true)) {
        this.$refs["startDate"].setDate(endDate)
        this.$refs["endDate"].setDate(withinRangeDate)
      }
    }
  }
}

export const computed = {
  computed: {
    startTimeOfToday() {
      return getDateWithStartTime()
    },
    endTimeOfTodayOnlySelectHours() {
      return getDateWithEndTime()
    },
    startDateEnabledRange() {
      const maxPreviousDate = getPreviousDateByDays(90)
      const endOfToday = getDateWithEndTime()

      return [{ from: maxPreviousDate, to: endOfToday }]
    },
    endDateEnabledRange() {
      const maxPreviousDate = getPreviousDateByDays(this.maxSearchDayRange)

      if (this.current.startDate > 0) {
        const rangeLimitDays = this.searchRangeDays

        const startDate = new Date(this.current.startDate)
        startDate.setHours(0, 0, 0, 0)

        const endDate = new Date(this.current.startDate)
        endDate.setDate(endDate.getDate() + rangeLimitDays)

        const today = getDateWithEndTime()
        const result = endDate.getTime() > today.getTime() ? today : endDate

        return [{ from: startDate, to: result }]
      } else {
        const endOfToday = getDateWithEndTime()

        return [{ from: maxPreviousDate, to: endOfToday }]
      }
    },
    dateRangeDays() {
      if ((this.current.startDate > 0) & (this.current.endDate > 0)) {
        const days = getDaysBetweenDates(this.current.startDate, this.current.endDate)
        return days
      }

      return null
    }
  }
}

export const methods = {
  methods: {
    validSearchTimeSelects() {
      const oneDay = 1000 * 60 * 60 * 24
      const diff = this.current.startDate - this.current.endDate
      // check start and end time
      const target = diff <= 0

      // check date range
      const days = Math.abs(diff / oneDay)

      if (target === true && days < this.searchRangeDays) {
        this.isSearchTimeDatesError = false
      } else {
        this.isSearchTimeDatesError = true
      }
    },
    setStartDate(payload) {
      const result = payload ? payload.getTime() : payload
      this.current.startDate = result
    },
    setEndDate(payload) {
      const result = payload ? payload.getTime() : payload
      this.current.endDate = result
    },
    getPayoutColorClass(value) {
      const targetValue = parseInt(value)

      return {
        "is-ok": targetValue > 0,
        "is-danger": targetValue < 0
      }
    },
    getPayoutFormatString(value) {
      return new Intl.NumberFormat("en-US", { maximumFractionDigits: 2, minimumFractionDigits: 2 }).format(value)
    },
    getLocaleNumberFormat(value) {
      return new Intl.NumberFormat("en-US", { maximumFractionDigits: 0, minimumFractionDigits: 0 }).format(value)
    },
    getCSSForQA(key, source) {
      const [target] = source.filter(x => x.name === key)
      return target.QA_CSS
    }
  }
}

export const ReportPageMixin = {
  ...watch,
  ...computed,
  ...methods
}

export default ReportPageMixin
