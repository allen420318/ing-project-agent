export const methods = {
  methods: {
    gameHallNameMapping(label) {
      const rules = [
        { regex: /(^轩辕厅)(.*)/, i18nKey: "IMMORTAL" },
        { regex: /(^仙侠厅)(.*)/, i18nKey: "WARRIOR" },
        { regex: /(^香榭厅)(.*)/, i18nKey: "CHAMPS" },
        { regex: /(^热浪厅)(.*)/, i18nKey: "ZEAL" },
        { regex: /(^幻境厅)(.*)/, i18nKey: "DREAM" },
        { regex: /(^佰丽厅)(.*)/, i18nKey: "BAILI" }
      ]

      const [result] = rules.filter(x => label.match(x.regex))

      if (result) {
        const [, , restString] = label.match(result.regex)

        return `${this.i18nSite.gameHall[result.i18nKey]} ${restString}`
      } else {
        return label
      }
    },
    gameTypeNameMapping(key) {
      const rules = new Map([
        ["BACCARAT", "BACCARAT"],
        ["DRAGONTIGER", "DRAGONTIGER"],
        ["SICBO", "SICBO"],
        ["FANTAN", "FANTAN"],
        ["ROULETTE", "ROULETTE"]
      ])

      const result = rules.get(key)

      if (result) {
        return `${this.i18nSite.gameType[result]}`
      } else {
        return null
      }
    },
    gameTableTypeNameMapping(key) {
      const rules = new Map([
        ["NORMAL", "NORMAL"],
        ["BLINK", "BLINK"],
        ["SPEEDY", "SPEEDY"]
      ])

      const result = rules.get(key)

      if (result) {
        return `${this.i18nSite.gameTableType[result]}`
      } else {
        return null
      }
    },
    orderStatusNameMapping(key) {
      const rules = new Map([
        [1, "NORMAL"],
        [2, "INTERRUPT"],
        [3, "REFUND_ORDER"],
        [4, "CHANGE_ORDER"]
      ])

      const result = rules.get(key)

      if (result) {
        return `${this.i18n.label[result]}`
      } else {
        return null
      }
    },
    transferTypeNameMapping(key) {
      const rules = new Map([
        [1, "TRASFER_IN"],
        [2, "TRASFER_OUT"],
        [3, "PLACE_BET"],
        [4, "PAYOUT"],
        [5, "CHANGE_ORDER"],
        [6, "REFUND_ORDER"],
        [7, "INTERRUPT"]
      ])

      const result = rules.get(key)

      if (result) {
        return `${this.i18n.label[result]}`
      } else {
        return null
      }
    },
    betAreaNameMapping(key) {
      const result = Object.keys(this.i18n.label).includes(key)

      if (result) {
        return `${this.i18n.label[key]}`
      } else {
        return null
      }
    }
  }
}
