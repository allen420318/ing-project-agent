export const GameResultMixin = {
  methods: {
    parseGameResult(data, gameType) {
      if (gameType === "BACCARAT") {
        const labels = new Map([
          [1, this.i18n.label.BANKER],
          [2, this.i18n.label.PLAYER],
          [3, this.i18n.label.TIE]
        ])
        const playerPoint = data[6]
        const bankerPoint = data[7]
        const winner = labels.get(data[8])

        return `${winner} ( ${this.i18n.label.PLAYER}${playerPoint} ${this.i18n.label.BANKER}${bankerPoint} )`
      } else if (gameType === "DRAGONTIGER") {
        const labels = new Map([
          [1, this.i18n.label.DRAGON],
          [2, this.i18n.label.TIGER],
          [3, this.i18n.label.TIE]
        ])
        const dragonPoints = data[2]
        const tigerPoints = data[3]
        const winner = labels.get(data[4])

        return `${winner} ( ${this.i18n.label.DRAGON}${dragonPoints} ${this.i18n.label.TIGER}${tigerPoints} )`
      } else if (gameType === "SICBO") {
        return data.slice(0, -1).join(",")
      } else {
        return data[0]
      }
    }
  }
}

export default GameResultMixin
