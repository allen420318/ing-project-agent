export const downloadCSV = (data, { filename = "file" } = {}) => {
  const a = document.createElement("a")
  a.textContent = "download"
  a.download = filename
  a.href = "data:text/csv;charset=utf-8,%EF%BB%BF" + encodeURIComponent(data)
  document.body.appendChild(a)
  a.click()
  document.body.removeChild(a)
}
