export const charFillUp = (value, counts, fill = "0") => {
  let result = ""

  if (value) {
    result = value.toString().substr(0, counts)
    result += fill.repeat(counts - result.length)
  } else {
    result = fill.repeat(counts)
  }

  return result
}

export const parseAPIString = data => {
  if (typeof data === "string") {
    const result = JSON.parse(data)

    if (Array.isArray(result) === true) {
      return [...result]
    } else {
      return result
    }
  } else {
    return data
  }
}
