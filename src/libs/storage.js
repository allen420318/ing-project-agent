/*----------------------------
	前端資料儲存與加密
------------------------------*/

export const getItem = function(key) {
  const data = localStorage.getItem(key)
  const defaultValue = {}

  try {
    if (data) {
      return JSON.parse(decodeURIComponent(escape(atob(data))))
    } else {
      return defaultValue
    }
  } catch (err) {
    // if string is not encoded correctly , clean up old string and return default value
    clear()
    return defaultValue
  }
}

export const setItem = function(key, data) {
  localStorage.setItem(key, btoa(unescape(encodeURIComponent(JSON.stringify(data)))))
}

const clear = key => {
  localStorage.removeItem(key)
}

const Storage = { setItem, getItem, clear }

export default Storage
