import store from "../store/index.js"

const timezone = store.state.timezone.currentTimezoneCode

export const getLocalTimezoneCode = () => {
  const target = new Date()
  const result = target.getTimezoneOffset() / 60
  return result
}

export const getPreviousDateByDays = (previousDays, specificDate = new Date()) => {
  const result = getDateByTimezone(new Date(specificDate))
  result.setDate(result.getDate() - previousDays)
  return result
}

export const getPreviousDateByMonthes = (previousMonths, specificDate = new Date()) => {
  const result = getDateByTimezone(new Date(specificDate))
  result.setMonth(result.getMonth() - previousMonths)
  return result
}

export const getFirstDateOfThisWeek = (specificDate = new Date()) => {
  const target = getDateByTimezone(specificDate)
  target.setDate(target.getDate() - target.getDay())
  return target
}

export const getFirstDateOfLastWeek = (specificDate = new Date()) => {
  const target = getDateByTimezone(specificDate)
  const diff = target.getDay() + 7
  target.setDate(target.getDate() - diff)
  return target
}

export const getLastDateOfLastWeek = (specificDate = new Date()) => {
  const date = getFirstDateOfLastWeek(specificDate)
  const diffDaysWithFirstDate = date.getDate() - date.getDay() + 6
  date.setDate(diffDaysWithFirstDate)
  return date
}

export const getFirstDateOfThisMonth = (specificDate = new Date()) => {
  const target = getDateByTimezone(specificDate)
  target.setFullYear(target.getFullYear(), target.getMonth(), 1)
  return target
}

export const getFirstDateOfLastMonth = (specificDate = new Date()) => {
  const target = getDateByTimezone(specificDate)
  target.setDate(0)
  target.setDate(1)
  return target
}

export const getLastDateOfLastMonth = (specificDate = new Date()) => {
  const target = getDateByTimezone(specificDate)
  target.setDate(0)
  return target
}

export const getDateWithStartTime = (specificDate = new Date()) => {
  const target = getDateByTimezone(specificDate)
  target.setHours(0, 0, 0, 0)
  return target
}

export const getDateWithEndTime = (specificDate = new Date()) => {
  const target = getDateByTimezone(specificDate)
  target.setHours(23, 59, 59, 999)
  return target
}

export const getDateWithHourSectionEndTime = (specificDate = new Date()) => {
  const target = getDateByTimezone(specificDate)
  target.setHours(target.getHours() - 1, 59, 59, 999)
  return target
}

export const formatToUTCTimeString = timeString => {
  const target = timeString.split(" ")
  return `${target[0]}T${target[1]}Z`
}

export const getLocalTimeString = (specificDate = new Date()) => {
  const target = getDateByTimezone(new Date(specificDate))

  const date = [target.getFullYear(), `${target.getMonth() + 1}`.padStart(2, 0), `${target.getDate()}`.padStart(2, 0)]
  const time = [
    `${target.getHours()}`.padStart(2, 0),
    `${target.getMinutes()}`.padStart(2, 0),
    `${target.getSeconds()}`.padStart(2, 0)
  ]
  return `${date.join("/")} ${time.join(":")}`
}

export const getAPIFormatTimeString = (specificDate = new Date(), { timezoneCode = timezone } = {}) => {
  const diff = Number(timezoneCode) * 60 * 60 * 1000

  const target = new Date(specificDate - diff)

  const date = [target.getFullYear(), `${target.getMonth() + 1}`.padStart(2, 0), `${target.getDate()}`.padStart(2, 0)]
  const time = [
    `${target.getHours()}`.padStart(2, 0),
    `${target.getMinutes()}`.padStart(2, 0),
    `${target.getSeconds()}`.padStart(2, 0)
  ]
  return `${date.join("/")} ${time.join(":")}`
}

export const getDateString = (specificDate = new Date(), isAddLeadingZero = true) => {
  const target = new Date(specificDate)
  if (isAddLeadingZero === true) {
    const date = [target.getFullYear(), `${target.getMonth() + 1}`.padStart(2, 0), `${target.getDate()}`.padStart(2, 0)]
    return date.join("-")
  } else {
    const date = [target.getFullYear(), `${target.getMonth() + 1}`, target.getDate()]
    return date.join("-")
  }
}

export const getDaysBetweenDates = (start, end) => {
  const oneDay = 1000 * 60 * 60 * 24
  const startDate = new Date(start)
  const endDate = new Date(end)

  return (startDate.getTime() - endDate.getTime()) / oneDay
}

export const getWithinDaysDate = (specificDate = new Date(), limitDays = 7, limitDate = new Date()) => {
  const target = new Date(specificDate)
  const limitTarget = new Date(limitDate)

  target.setDate(target.getDate() + limitDays)

  return target.getTime() >= limitDate.getTime() ? limitTarget : target
}

export const getDateByTimezone = (target, { fixedTimezone = timezone } = {}) => {
  let result = undefined

  if (Number.isInteger(target) === true) {
    result = new Date(target)
  } else {
    result = target
  }

  const timezoneOffset = Number(fixedTimezone)

  const offset = (result.getTimezoneOffset() + timezoneOffset * 60) * 60 * 1000
  result.setTime(result.getTime() + offset)

  return result
}