/* global __BuildVar__ */

const configRoutes = require(`./configs/${__BuildVar__.webType}/router.js`).default.router

const routes = configRoutes.filter(x => x.isEnabled === true)

export default routes

