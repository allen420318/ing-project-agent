const index = () => import("../../views/index.vue")
const PageNotFound = () => import("../../views/PageNotFound.vue")
const Login = () => import("../../views/Login.vue")
const Home = () => import("../../views/Home.vue")
const ForgetPassword = () => import("../../views/ForgetPassword.vue")
const ResetPassword = () => import("../../views/ResetPassword.vue")
const TransferRecords = () => import("../../views/TransferRecords.vue")
const FinanceStatistics = () => import("../../views/FinanceStatistics.vue")
const BasicAccountInfo = () => import("../../views/BasicAccountInfo.vue")
const SubAccountManage = () => import("../../views/SubAccountManage.vue")
const AbnormalRecords = () => import("../../views/AbnormalRecords.vue")
const CreditChanges = () => import("../../views/CreditChanges.vue")
const PermissionManage = () => import("../../views/PermissionManage.vue")

/**
 *  Page content is different on ELE & REAL environments
 */

const MemberBetRecords = () => import("../../views/projects/ele/MemberBetRecords.vue")
const GameRecords = () => import("../../views/projects/ele/GameRecords.vue")

const main = [
  {
    name: "index",
    path: "index.html",
    isEnabled: true,
    component: index,
    alias: ["/"]
  },
  {
    name: "TransferRecords",
    path: "TransferRecords.html",
    isEnabled: true,
    component: TransferRecords
  },
  {
    name: "FinanceStatistics",
    path: "FinanceStatistics.html",
    isEnabled: true,
    component: FinanceStatistics
  },
  {
    name: "MemberBetRecords",
    path: "MemberBetRecords.html",
    isEnabled: true,
    component: MemberBetRecords
  },
  {
    name: "GameRecords",
    path: "GameRecords.html",
    isEnabled: true,
    component: GameRecords
  },
  {
    name: "BasicAccountInfo",
    path: "BasicAccountInfo.html",
    isEnabled: true,
    component: BasicAccountInfo
  },
  {
    name: "SubAccountManage",
    path: "SubAccountManage.html",
    isEnabled: true,
    component: SubAccountManage
  },
  {
    name: "AbnormalRecords",
    path: "AbnormalRecords.html",
    isEnabled: true,
    component: AbnormalRecords
  },
  {
    name: "CreditChanges",
    path: "CreditChanges.html",
    isEnabled: true,
    component: CreditChanges
  },
  {
    name: "PermissionManage",
    path: "PermissionManage.html",
    isEnabled: true,
    component: PermissionManage
  }
]

const router = [
  {
    name: "Login",
    path: "/login.html",
    isEnabled: true,
    component: Login
  },
  {
    path: "/main",
    isEnabled: true,
    component: Home,
    children: main,
    alias: [""]
  },
  {
    name: "ForgetPassword",
    path: "/forgetPwd.html",
    isEnabled: true,
    component: ForgetPassword
  },
  {
    name: "ResetPassword",
    path: "/resetPassword.html",
    isEnabled: true,
    component: ResetPassword
  },
  {
    name: "PageNotFound",
    path: "*",
    isEnabled: true,
    component: PageNotFound
  }
]

export default {
  router
}
