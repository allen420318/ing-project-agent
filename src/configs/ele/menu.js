const menu = [
  {
    // 帳號管理
    key: "ACCOUNT_MANAGEMENT",
    routeName: "AccountManage",
    isActive: false,
    domId: {
      menu: "divAccountManageFuncSide",
      index: "labAccountManageFunc"
    },
    items: [
      {
        // 基本信息
        key: "BASIC_INFORMATION",
        routeName: "BasicAccountInfo",
        isActive: false,
        domId: {
          menu: "btnBasicInfoSide",
          index: "btnBasicInfo"
        },
        apiName: "basicInfo"
      },
      {
        // 子帳號管理
        key: "SUB_ACCOUNT_MANAGEMENT",
        routeName: "SubAccountManage",
        isActive: false,
        domId: {
          menu: "btnSubAccountSide",
          index: "btnSubAccount"
        },
        apiName: "subAgent"
      },
      {
        // 角色權限管理
        key: "PERMISSION_MANAGEMENT",
        routeName: "PermissionManage",
        isActive: false,
        domId: {
          menu: "btnCharPermissionSide",
          index: "btnCharPermission"
        },
        apiName: "permission"
      }
    ]
  },
  {
    // 遊戲管理
    key: "GAME_MANAGEMENT",
    routeName: "GameManage",
    isActive: false,
    domId: {
      menu: "divGameManageFuncSide",
      index: "labGameManageFunc"
    },
    items: [
      {
        // 遊戲紀錄
        key: "GAME_RECORDS",
        routeName: "GameRecords",
        isActive: false,
        domId: {
          menu: "btnGameRecordSide",
          index: "btnGameRecord"
        },
        apiName: "gameResults"
      }
    ]
  },
  {
    // 報表功能
    key: "REPORT_PAGES",
    routeName: "ReportPages",
    isActive: false,
    domId: {
      menu: "divReportFuncSide",
      index: "labReportFunc"
    },
    items: [
      {
        // 轉帳紀錄
        key: "TRANSFER_RECORDS",
        routeName: "TransferRecords",
        isActive: false,
        domId: {
          menu: "btnTransferRecordSide",
          index: "btnTransferRecord"
        },
        apiName: "tranRecords"
      },
      {
        // 帳務統計
        key: "ACCOUNTING_STATISTICS",
        routeName: "FinanceStatistics",
        isActive: false,
        domId: {
          menu: "btnTotalAccountingSide",
          index: "btnTotalAccounting"
        },
        apiName: "accountingStatistics"
      },
      {
        // 會員訂單
        key: "MEMBER_ORDERS",
        routeName: "MemberBetRecords",
        isActive: false,
        domId: {
          menu: "btnBetOrderSide",
          index: "btnBetOrder"
        },
        apiName: "gameBetLogs"
      },
      {
        // 帳變報表
        key: "CREDIT_CHANGE_REPORTS",
        routeName: "CreditChanges",
        isActive: false,
        domId: {
          menu: "btnAccountChangeSide",
          index: "btnAccountChange"
        },
        apiName: "accountingVariance"
      },
      {
        // 異常遊戲紀錄
        key: "ABNORMAL_GAME_RECORDS",
        routeName: "AbnormalRecords",
        isActive: false,
        domId: {
          menu: "btnBillAbnormalSide",
          index: "btnBillAbnormal"
        },
        apiName: "AbnormalGameRecords"
      }
    ]
  }
]

export default {
  menu
}
