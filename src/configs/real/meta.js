export default {
  siteEnvKey: "REAL",
  defaultLanguageCode: "cn",
  apiSettings: {
    endpoint: "/api/ibo",
    headers: {
      Accept: "application/json",
      "X-Requested-With": "XMLHttpRequest"
    },
    timeout: 60000
  }
}
