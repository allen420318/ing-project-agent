export default {
  siteEnvKey: "OVERLORD",
  defaultLanguageCode: "cn",
  apiSettings: {
    endpoint: "/api/dbo",
    headers: {
      Accept: "application/json",
      "X-Requested-With": "XMLHttpRequest"
    },
    timeout: 60000
  }
}
