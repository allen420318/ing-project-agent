import store from "../store/index.js"

export const checkRoutePermission = targetName => {
  const menu = store.state.menu.menu
  const exclusivePermissions = store.state.menu.topAgentExclusivePermissions
  const accountClassId = parseInt(store.state.member.accountClassId)

  const subMenuList = menu
    .flatMap(x => x.items)
    .filter(x => {
      // Top Agent & Overlord Agent
      if (accountClassId === 1 || accountClassId === 3) {
        return x
      } else {
        return !exclusivePermissions.includes(x.apiName)
      }
    })

  const [permissionCheckTarget] = subMenuList.filter(x => x.routeName === targetName)

  const { isActive } = permissionCheckTarget ? permissionCheckTarget : {}

  let result = false

  if (targetName === "index") {
    // Prevent url redirect loop
    result = undefined
  } else if (isActive === true) {
    result = true
  } else {
    result = false
  }

  return result
}
