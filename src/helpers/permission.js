import store from "../store/index.js"

export const applyMenuPermission = async () => {
  await store.dispatch("menu/setMenuPermissionList", {
    loginToken: store.state.member.loginToken,
    topAgentUid: store.state.member.topAgentUid,
    topAgentGroupUid: store.state.member.agentGroupUid,
    agentUid: store.state.member.agentUid,
    walletTypeId: store.state.member.walletTypeId
  })

  const menu = store.state.menu.menu

  // Process with sub menu
  const processedSubMenu = validSubMenu(menu, validSubMenuAPIPermissionHandler)

  // Process with root menu
  const finalMenu = validMenu(processedSubMenu)

  store.dispatch("menu/setMenuList", finalMenu)
}

const validSubMenuAPIPermissionHandler = subMenu => {
  const menuPermissionList = store.state.menu.menuPermissionList

  const result = menuPermissionList.includes(subMenu.apiName) === true

  const isActive = result === true

  return Object.assign({}, subMenu, { isActive })
}

const validSubMenu = (menu, handler) => {
  const result = menu.map(m => {
    const validSubMenu = m.items.map(handler)

    return Object.assign({}, m, { items: validSubMenu })
  })

  return result
}

const validMenu = menu => {
  const result = menu.map(m => {
    const displayMenuCount = m.items.filter(x => x.isActive === true).length

    const isActive = displayMenuCount > 0

    return Object.assign({}, m, { isActive })
  })

  return result
}
