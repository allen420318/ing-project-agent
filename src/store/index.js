import Vue from "vue"
import Vuex from "vuex"

import project from "./modules/project.js"
import member from "./modules/member.js"
import menu from "./modules/menu.js"
import error from "./modules/error.js"
import cache from "./modules/cache.js"
import i18n from "./modules/i18n.js"
import timezone from "./modules/timezone.js"

Vue.use(Vuex)

const store = new Vuex.Store({
  modules: {
    project,
    member,
    menu,
    error,
    cache,
    i18n,
    timezone
  },
  strict: process.env.NODE_ENV !== "production"
})

export default store
