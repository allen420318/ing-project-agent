import { LOAD_LANGUAGE_FILE, SET_DEFAULT_LANGUAGE_CODE } from "../mutation-types.js"
import Storage from "@/libs/storage.js"

const localStorageKey = "lang"

const isValidLanguageCode = code => {
  return state.aliavableLanguages.includes(code)
}

const isLanguageConfigExist = (languageCode = undefined) => {
  if (languageCode && isValidLanguageCode(languageCode)) {
    return true
  } else {
    return false
  }
}

const getValidLanguageCode = (languageCode = undefined, defaultLanguage) => {
  if (isLanguageConfigExist(languageCode) === true) {
    return languageCode
  } else {
    return defaultLanguage
  }
}

const state = {
  localStorageKey: localStorageKey,
  aliavableLanguages: ["cn", "tw", "en"],
  defaultLanguage: "cn",
  currentLanguage: undefined,
  data: {}
}

const actions = {
  async loadLanguageFile(
    { commit, state },
    { languageCode = undefined, isSwichBaseOnStorage = false, isAlwaysLoadFromCache = false } = {}
  ) {
    const isDataEmpty = Object.keys(state.data).length === 0

    if (isAlwaysLoadFromCache === true && isDataEmpty === false) return

    const metaData = Storage.getItem(state.localStorageKey)
    const targetCode = isSwichBaseOnStorage === true ? metaData.languageCode : languageCode
    const code = getValidLanguageCode(targetCode, state.defaultLanguage)

    const endpoint = `/i18n/${code}.json`
    const res = await fetch(endpoint, { headers: { Accept: "application/json", cache: "no-cache" } })

    Storage.setItem(state.localStorageKey, { languageCode: code })

    if (res.ok === false) {
      throw new Error(`${res.status} - Language file loading failed!`)
    } else {
      const result = await res.json()
      const payload = Object.assign({}, { languageCode: code }, { result })

      commit(LOAD_LANGUAGE_FILE, payload)
    }
  },
  setDefaultLanguageCode({ commit }, languageCode) {
    if (languageCode && languageCode.length === 2) {
      commit(SET_DEFAULT_LANGUAGE_CODE, languageCode)
    } else {
      throw new Error("i18n / Default language code error")
    }
  }
}

const mutations = {
  [LOAD_LANGUAGE_FILE](state, payload) {
    state.currentLanguage = payload.languageCode
    state.data = payload.result
  },
  [SET_DEFAULT_LANGUAGE_CODE](state, languageCode) {
    state.defaultLanguage = languageCode
  }
}

const getters = {
  apiErrorKeyMapper: state => {
    return new Map(Object.entries(state.data.Error).flatMap(x => Object.entries(x[1])))
  },
  languageLabelMap: state => {
    const list = [
      ["en", "English"],
      ["tw", "繁體中文"],
      ["cn", "简体中文"]
    ]

    return new Map(list.filter(x => state.aliavableLanguages.includes(x[0])))
  }
}

export default {
  namespaced: true,
  state,
  actions,
  mutations,
  getters
}
