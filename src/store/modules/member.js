import Storage from "../../libs/storage.js"
import { REMOVE_USER_LOGIN_DATA, SET_USER_LOGIN_DATA, SET_USER_ACCOUNT_META } from "../mutation-types.js"

const storeKey = "__member__"
const initValue = Storage.getItem(storeKey)

const state = {
  id: initValue.id || "",
  nickname: initValue.nickname || "",
  loginToken: initValue.loginToken || "",
  forceChangePassword: initValue.forceChangePassword || false,
  accountClassId: -1,
  agentGroupUid: -1,
  topAgentUid: -1,
  agentUid: -1,
  walletTypeId: -666,
  storeKey: storeKey,
  agentGroup: [],
  currencyList: []
}

const actions = {
  removeUserLoginData({ commit }) {
    commit(REMOVE_USER_LOGIN_DATA)
  },
  setUserLoginData({ commit }, data) {
    commit(SET_USER_LOGIN_DATA, data)
  },
  setUserAccountMeta({ commit }, data) {
    commit(SET_USER_ACCOUNT_META, data)
  }
}

const mutations = {
  [REMOVE_USER_LOGIN_DATA](state) {
    state.id = ""
    state.nickname = ""
    state.loginToken = ""
    state.forceChangePassword = ""
    state.accountClassId = -1
    state.topAgentUid = -1
    state.agentGroupUid = -1
    state.walletTypeId = -666
    Storage.clear(storeKey)
  },
  [SET_USER_LOGIN_DATA](state, { id, nickname, loginToken, forceChangePassword }) {
    state.id = id
    state.nickname = nickname
    state.loginToken = loginToken
    state.forceChangePassword = forceChangePassword
    Storage.setItem(storeKey, { id, nickname, loginToken, forceChangePassword })
  },
  [SET_USER_ACCOUNT_META](
    state,
    { accountClassId, topAgentUid, agentGroupUid, agentUid, walletTypeId, agentGroup, currencyList }
  ) {
    state.accountClassId = accountClassId
    state.topAgentUid = topAgentUid
    state.agentGroupUid = agentGroupUid
    state.agentUid = agentUid
    state.walletTypeId = walletTypeId
    // overlord only
    state.agentGroup = agentGroup
    state.currencyList = currencyList
  }
}

const getters = {
  isLogin: state => {
    return !(state.loginToken === "")
  },
  loginToken: state => {
    return state.loginToken
  },
  isOverlord: state => {
    return state.accountClassId === 3 || state.accountClassId === 4
  }
}

export default {
  namespaced: true,
  state,
  actions,
  mutations,
  getters
}
