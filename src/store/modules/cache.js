import {
  SET_MEMBER_BET_RECORDS_LIST_SEARCH_CONDITIONS_CACHE,
  REMOVE_MEMBER_BET_RECORDS_LIST_SEARCH_CONDITIONS_CACHE,
  SET_FINANCE_STATISTICS_LIST_SEARCH_CONDITIONS_CACHE,
  REMOVE_FINANCE_STATISTICS_LIST_SEARCH_CONDITIONS_CACHE,
  SET_CREDIT_CHANGES_LIST_SEARCH_CONDITIONS_CACHE,
  REMOVE_CREDIT_CHANGES_LIST_SEARCH_CONDITIONS_CACHE,
  SET_ABNORMAL_RECORDS_LIST_SEARCH_CONDITIONS_CACHE,
  REMOVE_ABNORMAL_RECORDS_LIST_SEARCH_CONDITIONS_CACHE,
  SET_TRANSFER_RECORDS_LIST_SEARCH_CONDITIONS_CACHE,
  REMOVE_TRANSFER_RECORDS_LIST_SEARCH_CONDITIONS_CACHE,
  SET_GAME_RECORDS_LIST_SEARCH_CONDITIONS_CACHE,
  REMOVE_GAME_RECORDS_LIST_SEARCH_CONDITIONS_CACHE
} from "../mutation-types.js"

const state = {
  searchConditions: {
    memberBetRecords: { list: {} },
    financeStatistics: { list: {} },
    creditChanges: { list: {} },
    abnormalRecords: { list: {} },
    transferRecords: { list: {} },
    gameRecords: { list: {} }
  }
}

const actions = {
  setMemberBetRecordsListSearchConditions({ commit }, payload) {
    commit(SET_MEMBER_BET_RECORDS_LIST_SEARCH_CONDITIONS_CACHE, payload)
  },
  removeMemberBetRecordsListSearchConditions({ commit }) {
    commit(REMOVE_MEMBER_BET_RECORDS_LIST_SEARCH_CONDITIONS_CACHE)
  },
  setFinanceStatisticsListSearchConditions({ commit }, payload) {
    commit(SET_FINANCE_STATISTICS_LIST_SEARCH_CONDITIONS_CACHE, payload)
  },
  removeFinanceStatisticsListSearchConditions({ commit }) {
    commit(REMOVE_FINANCE_STATISTICS_LIST_SEARCH_CONDITIONS_CACHE)
  },
  setCreditChangesListSearchConditions({ commit }, payload) {
    commit(SET_CREDIT_CHANGES_LIST_SEARCH_CONDITIONS_CACHE, payload)
  },
  removeCreditChangesListSearchConditions({ commit }) {
    commit(REMOVE_CREDIT_CHANGES_LIST_SEARCH_CONDITIONS_CACHE)
  },
  setAbnormalRecordsListSearchConditions({ commit }, payload) {
    commit(SET_ABNORMAL_RECORDS_LIST_SEARCH_CONDITIONS_CACHE, payload)
  },
  removeAbnormalRecordsListSearchConditions({ commit }) {
    commit(REMOVE_ABNORMAL_RECORDS_LIST_SEARCH_CONDITIONS_CACHE)
  },
  setTransferRecordsListSearchConditions({ commit }, payload) {
    commit(SET_TRANSFER_RECORDS_LIST_SEARCH_CONDITIONS_CACHE, payload)
  },
  removeTransferRecordsListSearchConditions({ commit }) {
    commit(REMOVE_TRANSFER_RECORDS_LIST_SEARCH_CONDITIONS_CACHE)
  },
  setGameRecordsListSearchConditions({ commit }, payload) {
    commit(SET_GAME_RECORDS_LIST_SEARCH_CONDITIONS_CACHE, payload)
  },
  removeGameRecordsListSearchConditions({ commit }) {
    commit(REMOVE_GAME_RECORDS_LIST_SEARCH_CONDITIONS_CACHE)
  }
}

const mutations = {
  [SET_MEMBER_BET_RECORDS_LIST_SEARCH_CONDITIONS_CACHE](state, payload) {
    state.searchConditions.memberBetRecords.list = payload
  },
  [REMOVE_MEMBER_BET_RECORDS_LIST_SEARCH_CONDITIONS_CACHE](state) {
    state.searchConditions.memberBetRecords.list = {}
  },
  [SET_FINANCE_STATISTICS_LIST_SEARCH_CONDITIONS_CACHE](state, payload) {
    state.searchConditions.financeStatistics.list = payload
  },
  [REMOVE_FINANCE_STATISTICS_LIST_SEARCH_CONDITIONS_CACHE](state) {
    state.searchConditions.financeStatistics.list = {}
  },
  [SET_CREDIT_CHANGES_LIST_SEARCH_CONDITIONS_CACHE](state, payload) {
    state.searchConditions.creditChanges.list = payload
  },
  [REMOVE_CREDIT_CHANGES_LIST_SEARCH_CONDITIONS_CACHE](state) {
    state.searchConditions.creditChanges.list = {}
  },
  [SET_ABNORMAL_RECORDS_LIST_SEARCH_CONDITIONS_CACHE](state, payload) {
    state.searchConditions.abnormalRecords.list = payload
  },
  [REMOVE_ABNORMAL_RECORDS_LIST_SEARCH_CONDITIONS_CACHE](state) {
    state.searchConditions.abnormalRecords.list = {}
  },
  [SET_TRANSFER_RECORDS_LIST_SEARCH_CONDITIONS_CACHE](state, payload) {
    state.searchConditions.transferRecords.list = payload
  },
  [REMOVE_TRANSFER_RECORDS_LIST_SEARCH_CONDITIONS_CACHE](state) {
    state.searchConditions.transferRecords.list = {}
  },
  [SET_GAME_RECORDS_LIST_SEARCH_CONDITIONS_CACHE](state, payload) {
    state.searchConditions.gameRecords.list = payload
  },
  [REMOVE_GAME_RECORDS_LIST_SEARCH_CONDITIONS_CACHE](state) {
    state.searchConditions.gameRecords.list = {}
  }
}

export default {
  namespaced: true,
  state,
  actions,
  mutations
}
