import { SET_SITE_TIMEZONE } from "../mutation-types.js"
import Storage from "@/libs/storage.js"

const localStorageKey = "timezone"

const state = {
  localStorageKey: localStorageKey,
  timezoneList: ["-4", "+8"],
  defaultTimezoneCode: "-4",
  currentTimezoneCode: undefined
}

const actions = {
  switchTimezone({ commit }, { timezoneCode = undefined, isBaseOnStorage = false } = {}) {
    const metaData = Storage.getItem(state.localStorageKey)
    const code = timezoneCode ? timezoneCode : state.defaultTimezoneCode
    const targetCode = isBaseOnStorage === true ? metaData : code

    Storage.setItem(state.localStorageKey, targetCode)

    commit(SET_SITE_TIMEZONE, targetCode)
  }
}

const mutations = {
  [SET_SITE_TIMEZONE](state, timezoneCode) {
    state.currentTimezoneCode = timezoneCode
  }
}

export default {
  namespaced: true,
  state,
  actions,
  mutations
}
