import { SET_PROJECT_TITLE, SET_PROJECT_API_SETTINGS } from "../mutation-types.js"

const state = {
  title: { key: "", separate: "" },
  api: {
    endpoint: "",
    headers: {},
    timeout: undefined
  }
}

const actions = {
  setProjectTitle({ commit }, data) {
    commit(SET_PROJECT_TITLE, data)
  },
  setAPISettings({ commit }, { endpoint, headers, timeout }) {
    commit(SET_PROJECT_API_SETTINGS, { endpoint, headers, timeout })
  }
}

const mutations = {
  [SET_PROJECT_TITLE](state, payload) {
    state.title.key = payload.key
    state.title.separate = payload.separate
  },
  [SET_PROJECT_API_SETTINGS](state, payload) {
    state.api.endpoint = payload.endpoint
    state.api.headers = payload.headers
    state.api.timeout = payload.timeout
  }
}

const getters = {
  getWebTitleWithSuffix: (state, getters) => {
    return ` ${state.title.separate} ${getters.getWebTitle}`
  },
  getWebTitle: (state, getters, rootState) => {
    const i18n = rootState.i18n.data.Site.env
    return i18n[state.title.key]
  }
}

export default {
  namespaced: true,
  state,
  actions,
  mutations,
  getters
}
