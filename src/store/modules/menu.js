import { SET_MENU_PERMISSION_LIST, SET_MENU_LIST } from "../mutation-types.js"

import { getAccountPermissionList } from "@/api/Permission.js"

const state = {
  menu: [],
  menuPermissionList: [],
  topAgentExclusivePermissions: ["permission", "subAgent"],
  singleWalletTypeExclusivePermissions: ["tranRecords", "accountingVariance"],
  overlordExclusivePermissions: ["basicInfo"]
}

const actions = {
  async setMenuPermissionList({ commit, getters }, payload) {
    const { exitCode, data } = await getAccountPermissionList(payload.loginToken, payload)

    if (exitCode === true && data.length > 0) {
      // Handle wallet type specific permissions
      const list = getters.getExcludedPermissions
      const result = data.filter(x => !list.includes(x))

      commit(SET_MENU_PERMISSION_LIST, result)
    } else {
      throw new Error("Account permission list request failed")
    }
  },
  setMenuList({ commit }, menu) {
    commit(SET_MENU_LIST, menu)
  }
}

const mutations = {
  [SET_MENU_PERMISSION_LIST](state, data) {
    state.menuPermissionList = data
  },
  [SET_MENU_LIST](state, data) {
    state.menu = data
  }
}

const getters = {
  getMenuList: state => {
    return state.menu.map(rootMenu => {
      const subMenu = rootMenu.items.map(sub => {
        return { apiName: sub.apiName, key: sub.key }
      })

      return { key: rootMenu.key, subMenu: subMenu }
    })
  },
  getExcludedPermissions: (state, getters, rootState) => {
    const walletTypeId = rootState.member.walletTypeId
    const acountClassId = rootState.member.accountClassId

    // Single wallet
    const singleWalletTypeExclusivePermissions = walletTypeId === 1 ? state.singleWalletTypeExclusivePermissions : []

    // Sub account
    const topAgentExclusivePermissions = acountClassId === 2 ? state.topAgentExclusivePermissions : []

    // Overlord like GPK ... etc
    const overlordExclusivePermissions = acountClassId === 3 ? state.overlordExclusivePermissions : []

    return [...singleWalletTypeExclusivePermissions, ...topAgentExclusivePermissions, ...overlordExclusivePermissions]
  },
  getMenuRouteNames: state => {
    return state.menu.flatMap(rootMenu => {
      const subMenu = rootMenu.items.map(sub => {
        return sub.routeName
      })

      return [rootMenu.routeName, ...subMenu]
    })
  }
}

export default {
  namespaced: true,
  state,
  actions,
  mutations,
  getters
}
