import {
  SET_FORCE_RESET_USER_PASSWORD_ERROR,
  REMOVE_FORCE_RESET_USER_PASSWORD_ERROR,
  SET_RESET_PASSWORD_TOKEN_ERROR,
  REMOVE_RESET_PASSWORD_TOKEN_ERROR
} from "../mutation-types.js"

const state = {
  forceResetPassword: { hasError: false, error: undefined },
  resetPasswordToken: { hasError: false, error: undefined }
}

const actions = {
  setForceResetPasswordError({ commit }, error) {
    commit(SET_FORCE_RESET_USER_PASSWORD_ERROR, error)
  },
  removeForceResetPasswordError({ commit }) {
    commit(REMOVE_FORCE_RESET_USER_PASSWORD_ERROR)
  },
  setResetPasswordTokenError({ commit }, error) {
    commit(SET_RESET_PASSWORD_TOKEN_ERROR, error)
  },
  removeResetPasswordTokenError({ commit }) {
    commit(REMOVE_RESET_PASSWORD_TOKEN_ERROR)
  }
}

const mutations = {
  [SET_FORCE_RESET_USER_PASSWORD_ERROR](state, error) {
    state.forceResetPassword.hasError = true
    state.forceResetPassword.error = error
  },
  [REMOVE_FORCE_RESET_USER_PASSWORD_ERROR](state) {
    state.forceResetPassword.hasError = false
    state.forceResetPassword.error = undefined
  },
  [SET_RESET_PASSWORD_TOKEN_ERROR](state, error) {
    state.resetPasswordToken.hasError = true
    state.resetPasswordToken.error = error
  },
  [REMOVE_RESET_PASSWORD_TOKEN_ERROR](state) {
    state.resetPasswordToken.hasError = false
    state.resetPasswordToken.error = undefined
  }
}

export default {
  namespaced: true,
  state,
  actions,
  mutations
}
