Vue.config.devtools = true

import "normalize.css"

import Vue from "vue"
import App from "@/app.vue"
import VueMeta from "vue-meta"

import GlobalStatesMixin from "./mixins/GlobalStates.js"
import ErrorMessageMixin from "./mixins/ErrorMessage.js"
import { checkVersion, getVersionFromUrl } from "./helpers/versionCheck.js"
import VeeValidate from "vee-validate"
import { Dialog, Button, MessageBox, Loading, Message, Popover } from "element-ui"
import store from "@/store/index.js"
import VueRouter from "vue-router"
import routes from "./routerSwitch.js"
import { applyMenuPermission } from "./helpers/permission.js"
import { checkRoutePermission } from "./helpers/routerGuard.js"
import { validateLoginToken } from "./api/User.js"

function getTimezoneCode() {
  const target = new Date()
  const result = Number(target.getTimezoneOffset() / 60) * -1

  if (result > 0) {
    return `+${result}`
  } else if (result < 0) {
    return result
  } else {
    return "+0"
  }
}

Vue.use(VueMeta)
Vue.use(VueRouter)

GlobalStatesMixin.methods.saveGlobalVariablesToVuex()

const router = new VueRouter({
  mode: "history",
  routes
})

router.beforeEach(async (to, from, next) => {
  const redirectTargetOnFailed = "/login.html"
  const routeTargetName = to.name
  const loginToken = store.state.member.loginToken
  const userAccount = store.state.member.id

  const notCheckLoginRouteNames = ["Login", "ForgetPassword", "ResetPassword", "PageNotFound", "DownloadFile"]

  try {
    await store.dispatch("i18n/loadLanguageFile", { isSwichBaseOnStorage: true, isAlwaysLoadFromCache: true })

    if (notCheckLoginRouteNames.includes(routeTargetName) === false) {
      // Check login token & retrive necessary data on every applied route
      const payload = { userAccount }
      const res = await validateLoginToken(loginToken, payload)

      if (res.exitCode !== true) {
        store.dispatch("member/removeUserLoginData")
        next(false)
        router.app.$destroy()
        window.location.replace(redirectTargetOnFailed)
      } else {
        // Save necessary data to vuex
        const [{ agentClassId, topAgentUid, agentGroupUid, agentUid, walletId, agentGroup, currencyList }] = res.data
        const payload = {
          accountClassId: agentClassId,
          topAgentUid,
          agentGroupUid,
          agentUid,
          walletTypeId: walletId,
          agentGroup,
          currencyList
        }
        store.dispatch("member/setUserAccountMeta", payload)

        if (store.getters["member/isOverlord"] === true) {
          store.dispatch("timezone/switchTimezone", { isBaseOnStorage: true })
        } else {
          store.dispatch("timezone/switchTimezone", { timezoneCode: getTimezoneCode(), isBaseOnStorage: false })
        }
      }

      // Check menu permission & rebuild
      await applyMenuPermission()

      const routePermissionResult = checkRoutePermission(routeTargetName)

      if (routePermissionResult === false) {
        const message = { message: "已無權限，請重整[確定]" }
        ErrorMessageMixin.methods.showErrorMessage(message, () => next({ name: "index" }))

        // Force break , do not go further
        return
      }
    }

    // You are free to go!
    next()
  } catch (err) {
    console.log(err)
    next(false)

    ErrorMessageMixin.methods.showErrorMessage(err, () => {
      store.dispatch("member/removeUserLoginData")
      router.app.$destroy()
      window.location.replace(redirectTargetOnFailed)
    })
  }
})

/*----------------------------
	vee-validate
------------------------------*/

Vue.use(VeeValidate, {
  events: "input|blur",
  classes: true,
  classNames: {
    valid: "is-valid",
    invalid: "is-invalid"
  }
})

/*----------------------------
	element-ui
------------------------------*/

Vue.use(Dialog)
Vue.use(Button)
Vue.use(Loading)
Vue.use(Popover)

Vue.prototype.$alert = MessageBox.alert
Vue.prototype.$message = Message
Vue.prototype.$Loading = Loading

new Vue({
  el: "#application",
  router,
  store,
  render: h => h(App),
  async created() {
    try {
      const versionFileLocation = "/version.txt"
      const result = await getVersionFromUrl(versionFileLocation)
      console.log("version : ", result)
      const isInSameVerion = checkVersion(result)
      isInSameVerion === false ? location.reload(true) : ""
    } catch (err) {
      console.log(err)
    }
  }
})
