const errorMessages = new Map([
  //UploadFile 上傳相關
  ["ResourceAliasNotExist", "資源別名(resourceAlias)尚未設定"],
  ["ResourceTypeMustAssign", "必須先指定資源類型"],
  ["ResourceHasSaved", "資料已移入儲存區"],
  ["FolderCannotBuild", "無法建立指定的目錄"],
  ["ResourceIsDeleted", "檔案已被刪除"],
  ["ResourceNotExist", "檔案不存在"],
  ["ResourceCannotDelete", "檔案無法刪除"]
])

export default errorMessages
