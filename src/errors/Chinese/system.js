const errorMessages = new Map([
  //System 系統
  ["ServiceNotExists", "服務不存在"],
  ["DataNoVerified", "資料未經過驗證"],
  ["PageNotExists", "網頁不存在"],
  ["EventNotExists", "事件不存在"],
  ["MustBeUseFunc", "必須使一個函式或方法"],
  ["DataFormatInvalid", "取得的資料格式不正確"],
  ["BusinessOpenTimeCannotModify", "營業時間不可修改"],
  ["TypeAliasIsNotExist", "全域設定類型別名不存在"],
  ["TokenNotExists", "Token不存在"],
  ["PermissionNotExists", "無此權限"],
  ["NotYetStartMaintain ", "請先進入全站維護才可執行次動作"],
  ["NotYetStartAgentMaintain  ", "請先進入代理維護才可執行次動作"],
  ["NotYetStartTableMaintain  ", "請先進入賭桌維護才可執行次動作"],
  ["FinishedMaintain", "維護已結束"],
  ["StartMaintain", "維護正在執行"],
  ["NoMaintain", "沒有要維護的項目"],
  ["ResetMaintain", "維護重啟中"],
  ["CanNotResetMaintain", "維護不可重啟"],
  ["MaintainExisted", "維護已存在"],
  ["MaintainNotice", "已通知維護即將開始"],
  ["MaintainNotNotice", "維護非已通知狀態"],
  ["CaptchaFail", "驗證碼驗證失敗"],
  ["CaptchaExpired", "驗證碼已過期"],
  ["UserIsNotTopAgent", "使用者不是最高代理"],
  ["DataInValid", "資料格式錯誤"],
  ["RoleNameIsBeenUsed", "角色名稱已被使用"]
])

export default errorMessages
