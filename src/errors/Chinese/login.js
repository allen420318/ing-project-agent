const errorMessages = new Map([
  //Login 登入相關
  ["VerifyPassword", "密碼輸入錯誤"],
  ["AccountIsDisabled", "此帳號已停用"],
  ["AccountIsLocked", "帳號已鎖定"],
  ["AccountNotExists", "無此帳號"],
  ["AccountIsBlocked", "限制登入"],
  ["DisabledByGroup", "最高代理(群組)已停用"],
  ["NotExistByGroup", "最高代理(群組)不存在"],
  ["ForceChangePassword", "強制變更密碼"],
  ["LoginTokenInvalid", "loginToken已失效"],
  ["Timeout", "登入逾時"],
  ["RepeatLogin", "重複登入"],
  ["PasswordDuplicateFromPast", "新密碼不可與舊密碼相同"],
  ["AccountFormatError", "帳號格式錯誤"],
  ["PasswordFormatError", "密碼格式錯誤"],
  ["PasswordFirstError", "密碼第一次錯誤"],
  ["PasswordSecondError", "密碼第二次錯誤"],
  ["SubAgentIsDisabled", "子帳號已被停用"]
])

export default errorMessages
