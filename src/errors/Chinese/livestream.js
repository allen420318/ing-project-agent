const errorMessages = new Map([
  // Livestream 直播相關
  ["LiveScenesIsClosed", "直播現場已關閉"],
  ["LiveScenesIsOpen", "直播現場已開啟"],
  ["LiveScenesNotExist", "直播現場不存在"],
  ["LevelOverTwenty", "超過20層限制"],
  ["LevelOneCannotDelete", "等級1只能編輯不能刪除"],
  ["LevelOneCannotModifyLevel", "等級1只能編輯其內容不可修改等級"],
  ["GiftBoxLessFive", "啟用的禮物盒數量少於5個"],
  ["GiftBoxOverEight", "啟用的禮物盒數量超過8個"]
])

export default errorMessages
