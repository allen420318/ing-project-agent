import AccountErrorMessages from "./account.js"
import GameErrorMessages from "./game.js"
import LivestreamMessages from "./livestream.js"
import LoginErrorMessages from "./login.js"
import RecordTableErrorMessages from "./recordTable.js"
import SystemErrorMessages from "./system.js"
import UploadErrorMessages from "./upload.js"

const main = new Map([
  ...AccountErrorMessages,
  ...GameErrorMessages,
  ...LivestreamMessages,
  ...LoginErrorMessages,
  ...RecordTableErrorMessages,
  ...SystemErrorMessages,
  ...UploadErrorMessages
])

export default main
