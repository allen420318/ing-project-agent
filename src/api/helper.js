export function getArrayFrom(count, step = 1) {
  return Array.from({ length: count }, (v, k) => k + step)
}

export function orderStatesBuilder(data, count = 4, step = 1) {
  return data.length > 0 ? data : getArrayFrom(count, step)
}

export function filterBuilder(data) {
  const gameHall = data.gameHall ? [data.gameHall] : []
  const gameType = data.gameType ? [data.gameType] : []
  const agentGroup = data.agentGroupUid ? [data.agentGroupUid] : []
  const agentGroupUid = data.agentGroupUid ? data.agentGroupUid : undefined
  const gameOrderState = orderStatesBuilder(data.gameOrderStates)

  return { filters: { themes: gameHall, state: gameOrderState, gameSets: gameType, agentGroup, agentGroupUid } }
}

export function paginationBuilder(data) {
  return { pagination: { pageSize: data.itemsPerPage, pageIndex: data.pagingIndex } }
}
