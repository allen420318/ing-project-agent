import fetcher from "@/libs/fetcher.js"

const WORKING_DOMAIN = "SystemManage"

export const getPermissionRoleList = async (loginToken, data) => {
  const result = await fetcher({
    ns: WORKING_DOMAIN,
    work: "Permission",
    version: "0.0.1",
    loginToken: loginToken,
    data: {
      command: "list",
      formData: {
        agentGroupUid: data.topAgentGroupUid
      }
    }
  })

  return result
}

export const getPermissionRoleDetail = async (loginToken, data) => {
  const result = await fetcher({
    ns: WORKING_DOMAIN,
    work: "Permission",
    version: "0.0.1",
    loginToken: loginToken,
    data: {
      command: "detail",
      formData: {
        agentGroupUid: data.topAgentGroupUid,
        permissionRoleUid: data.permissionRoleUid
      }
    }
  })

  return result
}

export const addPermissionRole = async (loginToken, data) => {
  const result = await fetcher({
    ns: WORKING_DOMAIN,
    work: "Permission",
    version: "0.0.1",
    loginToken: loginToken,
    data: {
      command: "add",
      formData: {
        name: data.permissionRoleName,
        remark: data.comment,
        group: data.permissions,
        agentGroupUid: data.topAgentGroupUid
      }
    }
  })

  return result
}

export const editPermissionRole = async (loginToken, data) => {
  const result = await fetcher({
    ns: WORKING_DOMAIN,
    work: "Permission",
    version: "0.0.1",
    loginToken: loginToken,
    data: {
      command: "modify",
      formData: {
        permissionRoleUid: data.permissionRoleUid,
        name: data.permissionRoleName,
        remark: data.comment,
        group: data.permissions,
        agentGroupUid: data.topAgentGroupUid
      }
    }
  })

  return result
}

export const deletePermissionRole = async (loginToken, data) => {
  const result = await fetcher({
    ns: WORKING_DOMAIN,
    work: "Permission",
    version: "0.0.1",
    loginToken: loginToken,
    data: {
      command: "delete",
      password: data.userPassword,
      formData: {
        permissionRoleUid: data.permissionRoleUid,
        agentGroupUid: data.topAgentGroupUid
      }
    }
  })

  return result
}

export const getAccountPermissionList = async (loginToken, data) => {
  const result = await fetcher({
    ns: "main",
    work: "Init",
    version: "0.0.1",
    loginToken: loginToken,
    data: {
      command: "menu",
      formData: {
        agentGroupUid: data.topAgentGroupUid,
        agentUid: data.agentUid,
        topAgentUid: data.topAgentUid
      }
    }
  })
  return result
}

export default {
  getPermissionRoleList,
  addPermissionRole,
  editPermissionRole,
  deletePermissionRole,
  getPermissionRoleDetail,
  getAccountPermissionList
}
