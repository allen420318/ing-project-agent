import fetcher from "@/libs/fetcher.js"

export const getGameHallList = async loginToken => {
  const result = await fetcher({
    ns: "reportManagement",
    work: "basicItems",
    version: "0.0.1",
    loginToken: loginToken,
    data: {
      dataTypes: ["themes"]
    }
  })

  return result
}
export const getGameTypeList = async loginToken => {
  const result = await fetcher({
    ns: "reportManagement",
    work: "basicItems",
    version: "0.0.1",
    loginToken: loginToken,
    data: {
      formData: {
        dataTypes: ["gameSetEffectiveList"]
      }
    }
  })

  return result
}

export const getGameStateList = async loginToken => {
  const result = await fetcher({
    ns: "reportManagement",
    work: "getGameResultState",
    version: "0.0.1",
    loginToken: loginToken
  })

  // Transform data
  const formatResult = { data: { gameResultState: [] } }

  for (const value of Object.values(result.data)) {
    formatResult.data.gameResultState.push({ ...value })
  }

  return formatResult
}

export const getGameOrderDetail = async (loginToken, payload) => {
  const result = await fetcher({
    ns: "reportManagement",
    work: "gameBetLogs",
    version: "0.0.1",
    loginToken: loginToken,
    data: {
      command: "serialDetails",
      formData: {
        betNo: payload.orderNumber
      }
    }
  })

  return result
}

export const getGameTransactionTypeList = async loginToken => {
  const result = await fetcher({
    ns: "reportManagement",
    work: "getTransferType",
    version: "0.0.1",
    loginToken: loginToken
  })

  return result
}

export default { getGameHallList, getGameTypeList, getGameStateList, getGameOrderDetail, getGameTransactionTypeList }
