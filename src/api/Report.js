import fetcher from "@/libs/fetcher.js"
import store from "@/store/index.js"
import { getAPIFormatTimeString } from "@/libs/time.js"
import { orderStatesBuilder, filterBuilder, paginationBuilder } from "./helper.js"

const WORKING_DOMAIN = "reportManagement"
const isOverlord = store.getters["member/isOverlord"]

// Member bet record

export const getOrdersByTimeRange = async (loginToken, data) => {
  const conditions = {
    conditions: {
      ...filterBuilder(data),
      where: {
        roundTime: {
          start: getAPIFormatTimeString(data.startDate),
          end: getAPIFormatTimeString(data.endDate)
        },
        account: data.playerAccount
      }
    }
  }

  const result = await fetcher({
    ns: WORKING_DOMAIN,
    work: "gameBetLogs",
    version: "0.0.1",
    loginToken: loginToken,
    data: {
      command: "report",
      formData: {
        name: isOverlord ? "DBOGameBetLogsByTime" : "IBOGameBetLogsByTime",
        ...paginationBuilder(data),
        ...conditions
      }
    }
  })

  return result
}

export const getOrdersByOrderNumber = async (loginToken, data) => {
  const conditions = {
    conditions: {
      filters: {},
      where: {
        betNo: data.gameOrderNumber
      }
    }
  }

  const result = await fetcher({
    ns: WORKING_DOMAIN,
    work: "gameBetLogs",
    version: "0.0.1",
    loginToken: loginToken,
    data: {
      command: "report",
      formData: {
        name: isOverlord ? "DBOGameBetLogs" : "IBOGameBetLogs",
        ...paginationBuilder(data),
        ...conditions
      }
    }
  })
  return result
}

export const getOrdersByTransactionNumber = async (loginToken, data) => {
  const conditions = {
    conditions: {
      filters: {},
      where: {
        serialNumber: data.orderTransactionNumber
      }
    }
  }

  const result = await fetcher({
    ns: WORKING_DOMAIN,
    work: "gameBetLogs",
    version: "0.0.1",
    loginToken: loginToken,
    data: {
      command: "report",
      formData: {
        name: isOverlord ? "DBOGameBetLogsBySerialNumber" : "IBOGameBetLogsBySerialNumber",
        ...paginationBuilder(data),
        ...conditions
      }
    }
  })
  return result
}

export const getOrdersByGameRound = async (loginToken, data) => {
  const conditions = {
    conditions: {
      ...filterBuilder(data),
      where: {
        shoeNo: data.gameShoesNumber,
        tableFrontId: data.gameDeskCode,
        roundNo: data.gameRoundNumber,
        account: data.playerAccount
      }
    }
  }

  const result = await fetcher({
    ns: WORKING_DOMAIN,
    work: "gameBetLogs",
    version: "0.0.1",
    loginToken: loginToken,
    data: {
      command: "report",
      formData: {
        name: isOverlord ? "DBOGameBetLogsByRoundNo" : "IBOGameBetLogsByRoundNo",
        ...paginationBuilder(data),
        ...conditions
      }
    }
  })
  return result
}

// Abnormal game records

export const getAbnormalByTimeRange = async (loginToken, data) => {
  const gameHall = data.gameHall ? [data.gameHall] : []
  const gameType = data.gameType ? [data.gameType] : []
  const agentGroup = data.agentGroupUid ? [data.agentGroupUid] : []
  const agentGroupUid = data.agentGroupUid ? data.agentGroupUid : undefined
  const gameOrderState = orderStatesBuilder(data.gameOrderStates, 3, 2)
  const gameTable = data.gameTable ? [data.gameTable] : []

  const conditions = {
    conditions: {
      filters: {
        themes: gameHall,
        state: gameOrderState,
        gameSets: gameType,
        agentGroup: agentGroup,
        tableSpeciesUid: gameTable,
        agentGroupUid
      },
      where: {
        roundTime: {
          start: getAPIFormatTimeString(data.startDate),
          end: getAPIFormatTimeString(data.endDate)
        }
      }
    }
  }

  const result = await fetcher({
    ns: WORKING_DOMAIN,
    work: "AbnormalGameRecords",
    version: "0.0.1",
    loginToken: loginToken,
    data: {
      command: "report",
      formData: {
        name: isOverlord ? "DBOAbnormalGameRecordsByDateTime" : "AbnormalGameRecordsByDateTime",
        ...paginationBuilder(data),
        ...conditions
      }
    }
  })

  return result
}

export const getAbnormalByGameRound = async (loginToken, data) => {
  const gameHall = data.gameHall ? [data.gameHall] : []
  const agentGroup = data.agentGroupUid ? [data.agentGroupUid] : []
  const gameOrderState = orderStatesBuilder(data.gameOrderStates, 3, 2)

  const conditions = {
    conditions: {
      filters: { themes: gameHall, state: gameOrderState, agentGroup: agentGroup },
      where: {
        shoeNo: data.gameShoesNumber,
        tableFrontId: data.gameDeskCode,
        roundNo: data.gameRoundNumber
      }
    }
  }

  const result = await fetcher({
    ns: WORKING_DOMAIN,
    work: "AbnormalGameRecords",
    version: "0.0.1",
    loginToken: loginToken,
    data: {
      command: "report",
      formData: {
        name: isOverlord ? "DBOAbnormalGameRecordsByRound" : "AbnormalGameRecordsByRound",
        ...paginationBuilder(data),
        ...conditions
      }
    }
  })
  return result
}

export const getAbnormalModifiedRecords = async (loginToken, data) => {
  const result = await fetcher({
    ns: "reportManagement",
    work: "AbnormalGameRecords",
    version: "0.0.1",
    loginToken: loginToken,
    data: {
      command: "details",
      gameRoundNo: data.gameRoundNumber,
      agentGroupUid: data.agentGroupUid
    }
  })
  return result
}

// Finance statistics

export const getFinanceStatisticOverview = async (loginToken, data) => {
  const conditions = {
    conditions: {
      filters: {
        groupUid: data.agentGroupUid ? [data.agentGroupUid] : [],
        agentGroupUid: data.agentGroupUid ? data.agentGroupUid : undefined,
        states: orderStatesBuilder(data.gameOrderStates)
      },
      where: {
        roundTime: {
          start: getAPIFormatTimeString(data.startDate),
          end: getAPIFormatTimeString(data.endDate)
        }
      }
    }
  }

  const result = await fetcher({
    ns: WORKING_DOMAIN,
    work: "accountingStatistics",
    version: "0.0.1",
    loginToken: loginToken,
    data: {
      command: "report",
      formData: {
        name: isOverlord ? "DBOAccountingStatistics" : "IBOAccountingStatistics",
        ...paginationBuilder(data),
        ...conditions
      }
    }
  })
  return result
}

export const getFinanceStatisticPerPlayer = async (loginToken, data) => {
  const conditions = {
    conditions: {
      filters: {
        agentGroupUid: data.agentGroupUid,
        states: orderStatesBuilder(data.gameOrderStates)
      },
      where: {
        roundTime: {
          start: getAPIFormatTimeString(data.startDate),
          end: getAPIFormatTimeString(data.endDate)
        },
        account: data.playerAccount
      }
    }
  }

  const result = await fetcher({
    ns: WORKING_DOMAIN,
    work: "playerAccountingStatistics",
    version: "0.0.1",
    loginToken: loginToken,
    data: {
      command: "report",
      formData: {
        name: isOverlord ? "DBOPlayerAccountingStatistics" : "IBOPlayerAccountingStatistics",
        ...paginationBuilder(data),
        ...conditions
      }
    }
  })
  return result
}

// Transfer records

export const getTransferRecordsByTimeRange = async (loginToken, data) => {
  const conditions = {
    conditions: {
      filters: {
        agentGroupUid: data.agentGroupUid ? data.agentGroupUid : undefined,
        transferTypes: data.transactionType ? [data.transactionType] : []
      },
      where: {
        timeRange: {
          start: getAPIFormatTimeString(data.startDate),
          end: getAPIFormatTimeString(data.endDate)
        },
        account: data.playerAccount
      }
    }
  }

  const result = await fetcher({
    ns: WORKING_DOMAIN,
    work: "tranRecords",
    version: "0.0.1",
    loginToken: loginToken,
    data: {
      command: "report",
      formData: {
        name: isOverlord ? "DBOTranRecord" : "IBOTranRecord",
        ...paginationBuilder(data),
        ...conditions
      }
    }
  })
  return result
}

export const getTransferRecordsByTransactionNumber = async (loginToken, data) => {
  const conditions = {
    conditions: {
      where: {
        billNumber: data.orderTransactionNumber
      }
    }
  }

  const result = await fetcher({
    ns: WORKING_DOMAIN,
    work: "tranRecords",
    version: "0.0.1",
    loginToken: loginToken,
    data: {
      command: "report",
      formData: {
        name: isOverlord ? "DBOTranRecordByBillNumber" : "IBOTranRecordByBillNumber",
        ...paginationBuilder(data),
        ...conditions
      }
    }
  })
  return result
}

// Credit changes

export const getCreditChangesOverview = async (loginToken, data) => {
  const conditions = {
    conditions: {
      filters: {
        agentGroupUid: data.agentGroupUid ? data.agentGroupUid : undefined
      },
      where: {
        roundTime: {
          start: getAPIFormatTimeString(data.startDate),
          end: getAPIFormatTimeString(data.endDate)
        },
        playerAccount: data.playerAccount
      }
    }
  }

  const result = await fetcher({
    ns: WORKING_DOMAIN,
    work: "accountingVariance",
    version: "0.0.1",
    loginToken: loginToken,
    data: {
      command: "report",
      formData: {
        name: isOverlord ? "DBOAccountingVariance" : "IBOAccountingVariance",
        ...paginationBuilder(data),
        ...conditions
      }
    }
  })
  return result
}

// Game records

export const getGameRecordsByTimeRange = async (loginToken, data) => {
  const gameHall = data.gameHall ? [data.gameHall] : []
  const gameType = data.gameType ? [data.gameType] : []
  const gameTable = data.gameTable ? [data.gameTable] : []

  const conditions = {
    conditions: {
      filters: {
        agentGroupUid: data.agentGroupUid ? data.agentGroupUid : undefined,
        themes: gameHall,
        gameSets: gameType,
        tableSpecies: gameTable
      },
      where: {
        roundTime: {
          start: getAPIFormatTimeString(data.startDate),
          end: getAPIFormatTimeString(data.endDate)
        }
      }
    }
  }

  const result = await fetcher({
    ns: WORKING_DOMAIN,
    work: "gameResults",
    version: "0.0.1",
    loginToken: loginToken,
    data: {
      command: "report",
      formData: {
        name: isOverlord ? "DBOGameResult" : "IBOGameResult",
        ...paginationBuilder(data),
        ...conditions
      }
    }
  })
  return result
}

export const getGameRecordsByGameRound = async (loginToken, data) => {
  const gameHall = data.gameHall ? [data.gameHall] : []

  const conditions = {
    conditions: {
      filters: { themes: gameHall },
      where: {
        tableFrontId: data.gameDeskCode,
        shoeNo: data.gameShoesNumber,
        roundNo: data.gameRoundNumber
      }
    }
  }

  const result = await fetcher({
    ns: WORKING_DOMAIN,
    work: "gameResults",
    version: "0.0.1",
    loginToken: loginToken,
    data: {
      command: "report",
      formData: {
        name: isOverlord ? "DBOGameResultByRoundNo" : "IBOGameResultByRoundNo",
        ...paginationBuilder(data),
        ...conditions
      }
    }
  })
  return result
}

export default {
  getOrdersByTimeRange,
  getOrdersByOrderNumber,
  getOrdersByTransactionNumber,
  getOrdersByGameRound,
  getAbnormalByTimeRange,
  getAbnormalByGameRound,
  getFinanceStatisticOverview,
  getFinanceStatisticPerPlayer,
  getTransferRecordsByTimeRange,
  getTransferRecordsByTransactionNumber,
  getCreditChangesOverview,
  getGameRecordsByTimeRange,
  getGameRecordsByGameRound
}
