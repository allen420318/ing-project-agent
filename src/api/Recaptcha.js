import { fetcher } from "@/libs/fetcher.js"
const endpoint = "api/captcha"

export const getRecaptcha = async () => {
  const result = await fetcher({
    endpoint: endpoint
  })

  return result
}

export default { getRecaptcha }
