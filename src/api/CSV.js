import fetcher from "@/libs/fetcher.js"
import { getAPIFormatTimeString } from "@/libs/time.js"
import { orderStatesBuilder } from "./helper.js"

const WORKING_DOMAIN = "reportManagement"

export const getMemberBetRecordsByTimeRangeCSV = async (loginToken, data) => {
  const config = { isText: true }

  const result = await fetcher(
    {
      ns: WORKING_DOMAIN,
      work: "gameBetLogs",
      version: "0.0.1",
      loginToken: loginToken,
      data: {
        command: "exportCSV",
        formData: {
          theme: data.gameHall ? data.gameHall : "",
          state: orderStatesBuilder(data.gameOrderStates),
          gameSet: data.gameType ? data.gameType : "",
          agentGroupUid: data.agentGroupUid ? data.agentGroupUid : undefined,
          roundTime: {
            start: getAPIFormatTimeString(data.startDate, { timezoneCode: data.timezoneCode }),
            end: getAPIFormatTimeString(data.endDate, { timezoneCode: data.timezoneCode })
          },
          account: data.playerAccount,
          timezone: data.timezoneCode
        }
      }
    },
    config
  )

  return result
}

export const getTransferRecordsByTimeRangeCSV = async (loginToken, data) => {
  const config = { isText: true }
  console.log(data)
  console.log({
    ns: WORKING_DOMAIN,
    work: "tranRecords",
    version: "0.0.1",
    loginToken: loginToken,
    data: {
      command: "exportCSV",
      formData: {
        agentGroupUid: data.agentGroupUid ? data.agentGroupUid : undefined,
        transferType: data.transactionType ? data.transactionType : "",
        roundTime: {
          start: getAPIFormatTimeString(data.startDate, { timezoneCode: data.timezoneCode }),
          end: getAPIFormatTimeString(data.endDate, { timezoneCode: data.timezoneCode })
        },
        account: data.playerAccount,
        timezone: data.timezoneCode
      }
    }
  })
  const result = await fetcher(
    {
      ns: WORKING_DOMAIN,
      work: "tranRecords",
      version: "0.0.1",
      loginToken: loginToken,
      data: {
        command: "exportCSV",
        formData: {
          agentGroupUid: data.agentGroupUid ? data.agentGroupUid : undefined,
          transferType: data.transactionType ? data.transactionType : "",
          roundTime: {
            start: getAPIFormatTimeString(data.startDate, { timezoneCode: data.timezoneCode }),
            end: getAPIFormatTimeString(data.endDate, { timezoneCode: data.timezoneCode })
          },
          account: data.playerAccount,
          timezone: data.timezoneCode
        }
      }
    },
    config
  )

  return result
}
