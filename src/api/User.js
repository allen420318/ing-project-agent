import fetcher from "@/libs/fetcher.js"

const WORKING_DOMAIN = "main"

export const validateLoginToken = async (loginToken, data) => {
  const result = await fetcher({
    ns: WORKING_DOMAIN,
    work: "noop",
    version: "0.0.1",
    loginToken: loginToken,
    data: { command: "identity", name: data.userAccount }
  })
  return result
}

export const forceResetPassword = async data => {
  const result = await fetcher({
    ns: "main",
    work: "forceChangePassword",
    version: "0.0.1",
    loginToken: data.loginToken,
    data: {
      password: data.newPassword
    }
  })
  return result
}

export const resetPasswordByToken = async data => {
  const result = await fetcher({
    ns: "main",
    work: "changePasswordByToken",
    version: "0.0.1",
    data: {
      password: data.newPassword,
      token: data.resetPasswordToken
    }
  })
  return result
}

export const verifyResetPasswordToken = async resetPasswordToken => {
  const result = await fetcher({
    ns: "main",
    work: "forgetPasswordTokenVerify",
    version: "0.0.1",
    data: {
      token: resetPasswordToken
    }
  })
  return result
}

export const sendPasswordResetLink = async email => {
  const result = await fetcher({
    ns: "main",
    work: "forgetPassword",
    version: "0.0.1",
    data: {
      email: email
    }
  })
  return result
}

export const userLogin = async data => {
  const result = await fetcher({
    ns: "main",
    work: "login",
    version: "0.0.1",
    captchaToken: data.captchaToken,
    captchaValue: data.captchaValue,
    data: {
      id: data.id,
      password: data.pwd,
      tz: Math.abs(new Date().getTimezoneOffset())
    }
  })
  return result
}

export const userLogout = async loginToken => {
  const result = await fetcher({
    ns: "main",
    work: "logout",
    version: "0.0.1",
    loginToken: loginToken
  })
  return result
}

export const changePassword = async data => {
  const result = await fetcher({
    ns: "main",
    work: "changePassword",
    version: "0.0.1",
    loginToken: data.loginToken,
    data: {
      password: data.oldPassword,
      formData: {
        password: data.newPassword
      }
    }
  })
  return result
}

export const getAccountRole = async (loginToken, data) => {
  const result = await fetcher({
    ns: "main",
    work: "subAgent",
    version: "0.0.1",
    loginToken: loginToken,
    data: {
      command: "identity",
      name: data.userAccount
    }
  })
  return result
}

export const getAccountMetaInfo = async (loginToken, data) => {
  const result = await fetcher({
    ns: "reportManagement",
    work: "basicInfo",
    version: "0.0.1",
    loginToken: loginToken,
    data: {
      command: "info",
      account: data.userAccount,
      classId: data.accountClassId
    }
  })
  return result
}
