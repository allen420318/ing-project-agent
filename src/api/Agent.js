import fetcher from "@/libs/fetcher.js"

export const getTopAgentList = async loginToken => {
  const result = await fetcher({
    ns: "reportManagement",
    work: "reportPermission",
    version: "0.0.1",
    loginToken: loginToken
  })

  // Transform data
  const formatResult = { data: [] }

  for (const [key, value] of Object.entries(result.data)) {
    formatResult.data.push({ groupUid: key, account: value.account })
  }

  return formatResult
}

export const getSubAccountList = async (loginToken, data) => {
  const result = await fetcher({
    ns: "main",
    work: "subAgent",
    version: "0.0.1",
    loginToken: loginToken,
    data: {
      command: "subList",
      topAgentUid: data.topAgentUid
    }
  })

  return result
}

export const addSubAccount = async (loginToken, data) => {
  const result = await fetcher({
    ns: "main",
    work: "subAgent",
    version: "0.0.1",
    loginToken: loginToken,
    data: {
      command: "add",
      account: data.subAccountName,
      nickname: data.nickname,
      password: data.password,
      email: data.email,
      topAgentUid: data.topAgentUid,
      permissionRoleUidList: data.permissionRoleUidList
    }
  })

  return result
}

export const updateSubAccount = async (loginToken, data) => {
  const result = await fetcher({
    ns: "main",
    work: "subAgent",
    version: "0.0.1",
    loginToken: loginToken,
    data: {
      command: "update",
      nickname: data.nickname,
      uid: data.subAccountUid,
      email: data.email,
      topAgentUid: data.topAgentUid,
      permissionRoleUidList: data.permissionRoleUidList
    }
  })

  return result
}

export const deleteSubAccount = async (loginToken, data) => {
  const result = await fetcher({
    ns: "main",
    work: "subAgent",
    version: "0.0.1",
    loginToken: loginToken,
    data: {
      command: "delete",
      uid: data.subAccountUid,
      password: data.userPassword,
      topAgentUid: data.topAgentUid
    }
  })

  return result
}

export const unlockSubAccount = async (loginToken, data) => {
  const result = await fetcher({
    ns: "main",
    work: "subAgent",
    version: "0.0.1",
    loginToken: loginToken,
    data: {
      command: "unlock",
      uid: data.subAccountUid,
      topAgentUid: data.topAgentUid
    }
  })

  return result
}

export const enableSubAccount = async (loginToken, data) => {
  const result = await fetcher({
    ns: "main",
    work: "subAgent",
    version: "0.0.1",
    loginToken: loginToken,
    data: {
      command: "enable",
      uid: data.subAccountUid,
      topAgentUid: data.topAgentUid
    }
  })

  return result
}

export const disableSubAccount = async (loginToken, data) => {
  const result = await fetcher({
    ns: "main",
    work: "subAgent",
    version: "0.0.1",
    loginToken: loginToken,
    data: {
      command: "disable",
      uid: data.subAccountUid,
      topAgentUid: data.topAgentUid
    }
  })

  return result
}

export default {
  getTopAgentList,
  getSubAccountList,
  addSubAccount,
  updateSubAccount,
  deleteSubAccount,
  unlockSubAccount,
  enableSubAccount,
  disableSubAccount
}
